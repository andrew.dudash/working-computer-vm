#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>
#include <readline/readline.h>
#include <ncurses.h>

#define MEMORY_SIZE 4096
#define SCREEN_MEMORY_OFFSET 256
#define SCREEN_WIDTH 80
#define SCREEN_HEIGHT 25
#define CURSOR_X_MEMORY_OFFSET 255
#define CURSOR_Y_MEMORY_OFFSET 254
#define REGISTER_HEIGHT 7
#define REGISTER_WIDTH 12

int is_alu_instruction(uint8_t instruction) {
  // 0b0000_ooss
  return (instruction >> 4) == 0x0;
}

int is_jump_instruction(uint8_t instruction) {
  // 0b0001_oooo
  return (instruction >> 4) == 0x1;
}

int is_jump_zero_instruction(uint8_t instruction) {
  // 0b1110_oooo
  return (instruction >> 4) == 0xe;
}

int is_move_register_instruction(uint8_t instruction) {
  // 0b0010_ddss
  return (instruction >> 4) == 0x2;
}

int is_move_immediate_instruction(uint8_t instruction) {
  // 0b0100_iiii
  return (instruction >> 4) == 0x4;
}

int is_halt_instruction(uint8_t instruction) {
  return (instruction >> 4) == 0xf;
}

int is_store_memory_instruction(uint8_t instruction) {
  return (instruction >> 4) == 0xc;
}

int is_load_memory_instruction(uint8_t instruction) {
  return (instruction >> 4) == 0xd;
}

uint8_t * load(FILE * file) {
  uint8_t * memory = calloc(sizeof(uint8_t), MEMORY_SIZE);
  if (memory == NULL) {
    perror(NULL);
    return NULL;
  }
  size_t read_count;
  uint8_t * memory_offset = memory;
  do {
    read_count = fread(memory_offset, MEMORY_SIZE - read_count, sizeof(uint8_t), file);
    memory_offset += read_count;
  } while(read_count != 0);
  return memory;
}

struct machine_state {
  uint8_t register_table[4];
  size_t instruction_pointer;
};

void dump_register_table(struct machine_state * state) {
  uint8_t * register_table = state->register_table;
  printf("IP:\t%02x\n", state->instruction_pointer);
  printf("ACCUM:\t%02x\n", register_table[0]);
  printf("B:\t%02x\n", register_table[1]);
  printf("C:\t%02x\n", register_table[2]);
  printf("D:\t%02x\n", register_table[3]);
}

void dump_register_table_tui(WINDOW * window, int y, int x, struct machine_state * state) {
  uint8_t * register_table = state->register_table;
  mvwprintw(window, y + 0, x, "IP:\t%02x", state->instruction_pointer);
  mvwprintw(window, y + 1, x, "ACCUM:\t%02x", register_table[0]);
  mvwprintw(window, y + 2, x, "B:\t%02x", register_table[1]);
  mvwprintw(window, y + 3, x, "C:\t%02x", register_table[2]);
  mvwprintw(window, y + 4, x, "D:\t%02x", register_table[3]);
}

uint8_t * machine_state_accum(struct machine_state * state) {
  return state->register_table + 0;
}

uint8_t * machine_state_b(struct machine_state * state) {
  return state->register_table + 1;
}

uint8_t * machine_state_c(struct machine_state * state) {
  return state->register_table + 2;
}

uint8_t * machine_state_d(struct machine_state * state) {
  return state->register_table + 3;
}

struct machine_state create_machine_state() {
  struct machine_state state = {
    .register_table = {0xff, 0xff, 0xff, 0xff},
    .instruction_pointer = 0
  };
  return state;
}

void execute_instruction(uint8_t instruction, struct machine_state * state, uint8_t * memory) {
  uint8_t * accum = machine_state_accum(state);
  uint8_t * b = machine_state_b(state);
  uint8_t * c = machine_state_c(state);
  uint8_t * d = machine_state_d(state);
  size_t * instruction_pointer = &(state->instruction_pointer);
  if (is_alu_instruction(instruction)) {
    uint8_t operand = state->register_table[instruction & 0x3];
    uint8_t operation = (instruction >> 2) & 0x3;
    if (operation == 0x0) {
      *accum += operand;
    } else if (operation == 0x1) {
      *accum -= operand;
    }
  } else if (is_jump_instruction(instruction)) {
    int8_t offset = instruction & 0xf;
    if (offset & 0x8) {
      offset |= 0xf0;
    }
    *instruction_pointer = (ssize_t)(*instruction_pointer) + offset;
  } else if (is_jump_zero_instruction(instruction)) {
    int8_t offset = instruction & 0xf;
    if (offset & 0x8) {
      offset |= 0xf0;
    }
    if (*accum == 0) {
      *instruction_pointer = (ssize_t)(*instruction_pointer) + offset;
    }
  } else if (is_move_register_instruction(instruction)) {
    uint8_t * source = state->register_table + ((instruction >> 0) & 0x03);
    uint8_t * destination = state->register_table + ((instruction >> 2) & 0x03);
    *destination = *source;
  } else if (is_move_immediate_instruction(instruction)) {
    uint8_t immediate = (instruction >> 0) & 0xf;
    *accum = immediate;
  } else if (is_halt_instruction(instruction)) {
    (*instruction_pointer)--;
  } else if (is_store_memory_instruction(instruction)) {
    memory[(0xf & instruction) << 8 | (size_t)*d] = *accum;
    (*d)++;
  } else if (is_load_memory_instruction(instruction)) {
    *accum = memory[(0xf & instruction) << 8 | (size_t)*c];
    (*c)++;
  } else { // GENERAL EXCEPTION
    *instruction_pointer = 4096;
  }    
  // Increment instruction pointer.
  (*instruction_pointer)++;
  if (*instruction_pointer == 4096) {
    *instruction_pointer = 0;
  }
}

void execute_program(uint8_t * memory) {
  WINDOW * console_window = newwin(SCREEN_HEIGHT + 2, SCREEN_WIDTH + 2, 1, 1);
  WINDOW * register_window = newwin(REGISTER_HEIGHT, REGISTER_WIDTH, 1, SCREEN_WIDTH + 3);
  struct machine_state state = create_machine_state();
  for (;;) {
    uint8_t instruction = memory[state.instruction_pointer];    
    execute_instruction(instruction, &state, memory);
    erase();
    for (size_t row = 0; row < SCREEN_HEIGHT; row++) {
      for (size_t column = 0; column < SCREEN_WIDTH; column++) {
	move(row + 1, column + 1);
	uint8_t screen_character = memory[SCREEN_MEMORY_OFFSET + (row * SCREEN_WIDTH) + column];
	if (!isprint(screen_character)) {
	  screen_character = 0xff;
	}
	wprintw(console_window, "%c", screen_character);
      }
      wprintw(console_window, "\n");
    }
    box(console_window, 0, 0);

    dump_register_table_tui(register_window, 1, 1, &state);
    box(register_window, 0, 0);
    
    wmove(console_window, (memory[CURSOR_Y_MEMORY_OFFSET] % SCREEN_HEIGHT) + 1, (memory[CURSOR_X_MEMORY_OFFSET] % SCREEN_WIDTH) + 1);
    usleep(62 * 1000);
    wrefresh(console_window);
    wrefresh(register_window);
  }
}

void debug_program(uint8_t * memory) {
  struct machine_state state = create_machine_state();
  size_t print_offset;
  printf("Program Started\n");
  for (;;) {
    char * command = readline("WAITING: ");
    if ((strcmp(command, "continue") == 0) || (strcmp(command, "c") == 0)) {
      uint8_t instruction = memory[state.instruction_pointer];    
      execute_instruction(instruction, &state, memory);
      dump_register_table(&state);
      if (is_halt_instruction(instruction)) {
	printf("Program Halted\n");
	continue;
      }
    } else if ((strcmp(command, "quit") == 0) || (strcmp(command, "q") == 0)) {
      free(command);
      break;
    } else if (sscanf(command, "print %zi", &print_offset) == 1) {      
      printf("%zx: %x\n", print_offset, memory[print_offset]);
    } else {
      printf("Command not recognized: %s\n", command);
    }
    if (command) {
      free(command);
    }
  }
}

int main(int argc, char ** argv) {
  if (argc != 2) {
    return -1;
  }
  FILE * file = fopen(argv[1], "r");
  if (file == NULL) {
    perror(NULL);
    return -1;
  }
  uint8_t * memory = load(file);
  fclose(file);
  initscr();
  execute_program(memory);
  endwin();
  free(memory);
  return 0;
}
