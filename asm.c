#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>

#define MEMORY_SIZE 4096

enum reg {
  ACCUM,
  B,
  C,
  D,
  INVALID_REG
};

static uint8_t set_source_register(uint8_t instruction, enum reg source) {
  switch (source) {
  case ACCUM:
    instruction |= 0x00;
    break;
  case B:
    instruction |= 0x01;
    break;
  case C:
    instruction |= 0x02;
    break;
  case D:
    instruction |= 0x03;
    break;
  default:
    break;
  }
  return instruction;
}

static uint8_t set_destination_register(uint8_t instruction, enum reg source) {
  switch (source) {
  case ACCUM:
    instruction |= (0x00 << 2);
    break;
  case B:
    instruction |= (0x01 << 2);
    break;
  case C:
    instruction |= (0x02 << 2);
    break;
  case D:
    instruction |= (0x03 << 2);
    break;
  default:
    break;
  }
  return instruction;
}

uint8_t build_add_instruction(enum reg source) {
  return set_source_register((0x0 << 4) | 0x0, source);
}

uint8_t build_sub_instruction(enum reg source) {
  return set_source_register((0x0 << 4) | 0x4, source);
}

uint8_t build_jmp_instruction(size_t offset) {
  return (0x1 << 4) | (offset & 0xf);
}

uint8_t build_jz_instruction(size_t offset) {
  return (0xe << 4) | (offset & 0xf);
}

uint8_t build_mov_register_instruction(enum reg source, enum reg destination) {
  uint8_t instruction = 0x20;
  instruction = set_source_register(instruction, source);
  instruction = set_destination_register(instruction, destination);
  return instruction;
}

uint8_t build_mov_immediate_instruction(size_t immediate) {
  uint8_t instruction = 0x40;
  return instruction | immediate;
}

uint8_t build_str_instruction(size_t offset) {
  return (0xc << 4) | (0xf & offset);
}

uint8_t build_lod_instruction(size_t offset) {
  return (0xd << 4) | (0xf & offset);
}

enum reg str2reg(char * str) {
  if (strcmp(str, "accum") == 0) {
    return ACCUM;
  } else if (strcmp(str, "b") == 0) {
    return B;
  } else if (strcmp(str, "c") == 0) {
    return C;
  } else if (strcmp(str, "d") == 0) {
    return D;
  } else {
    return INVALID_REG;
  }
}

int read_add_instruction(char * line, size_t line_number, uint8_t * instruction) {
  char source_buffer[1024];
  if (sscanf(line, "add %[^\n, \t]\n", source_buffer) != 1) {
    return 1;
  }  
  enum reg source = str2reg(source_buffer);
  if (source == INVALID_REG) {
    fprintf(stderr, "Line %zi: Unknown source register: %s\n", line_number, source_buffer);
    return 1;
  }
  *instruction = build_add_instruction(source);
  return 0;
}

int read_sub_instruction(char * line, size_t line_number, uint8_t * instruction) {
  char source_buffer[1024];
  if (sscanf(line, "sub %[^\n, \t]", source_buffer) != 1) {
    return 1;
  }  
  enum reg source = str2reg(source_buffer);
  if (source == INVALID_REG) {
    fprintf(stderr, "Line %zi: Unknown source register: %s\n", line_number, source_buffer);
    return 1;
  }
  *instruction = build_sub_instruction(source);
  return 0;
}

int read_jmp_instruction(char * line, size_t line_number, uint8_t * instruction) {
  ssize_t offset;
  if (sscanf(line, "jmp %zi", &offset) != 1) {
    return 1;
  }
  if (offset > 127) {
    fprintf(stderr, "Line %zi: Invalid jump offset\n", line_number);
    return 1;
  }
  *instruction = build_jmp_instruction(offset);
  return 0;
}

int read_jz_instruction(char * line, size_t line_number, uint8_t * instruction) {
  ssize_t offset;
  if (sscanf(line, "jz %zi", &offset) != 1) {
    return 1;
  }
  if (offset > 127) {
    fprintf(stderr, "Line %zi: Invalid jump offset\n", line_number);
    return 1;
  }
  *instruction = build_jz_instruction(offset);
  return 0;
}

int read_mov_register_instruction(char * line, size_t line_number, uint8_t * instruction) {
  char destination_buffer[1024];
  char source_buffer[1024];
  sscanf(line, "movr %[^\n, \t], %[^\n, \t]", destination_buffer, source_buffer);
  enum reg destination = str2reg(destination_buffer);
  if (destination == INVALID_REG) {
    fprintf(stderr, "Line %zi: Unknown destination register: %s\n", line_number, destination_buffer);
    return 1;
  }
  enum reg source = str2reg(source_buffer);
  if (source == INVALID_REG) {
    fprintf(stderr, "Line %zi: Unknown source register: %s\n", line_number, source_buffer);
    return 1;
  }
  *instruction = build_mov_register_instruction(source, destination);
  return 0;
}

int read_mov_immediate_instruction(char * line, size_t line_number, uint8_t * instruction) {
  size_t immediate;
  if (sscanf(line, "movi %zi", &immediate) != 1) {
    return 1;
  }
  *instruction = build_mov_immediate_instruction(immediate);
  return 0;
}


int read_str_instruction(char * line, size_t line_number, uint8_t * instruction) {
  size_t offset;
  if (sscanf(line, "str %zi", &offset) != 1) {
    printf("Line %zi: Could not parse offset in line: %s\n", line_number, line);
    return 1;
  }
  *instruction = build_str_instruction(offset);
  return 0;
}

int read_lod_instruction(char * line, size_t line_number, uint8_t * instruction) {
  size_t offset;
  if (sscanf(line, "lod %zi", &offset) != 1) {
    return 1;
  }
  *instruction = build_lod_instruction(offset);
  return 0;
}

int read_db(char * line, size_t line_number, uint8_t * instruction) {
  size_t offset;
  if (sscanf(line, "db %zi", &offset) != 1) {
    return 1;
  }
  *instruction = offset;
  return 0;
}

uint8_t * assemble_file(FILE * in_file) {
  char line_buffer[1024];
  uint8_t * memory = calloc(MEMORY_SIZE, sizeof(uint8_t));
  size_t memory_count = 0;
  size_t line_number = 0;
  for (;;) {
    if (fgets(line_buffer, sizeof(line_buffer), in_file) == NULL) {
      break;
    }
    if (memory_count == MEMORY_SIZE) {
      fprintf(stderr, "Line %zi: Program is too big to load.", line_number);
    }
    line_number++;
    
    uint8_t instruction;
    if (strncmp(line_buffer, "add", 3) == 0) {
      if (read_add_instruction(line_buffer, line_number, &instruction) != 0) {
	fprintf(stderr, "Line %zi: Could not understand: %s\n", line_number, line_buffer);
	break;
      }
    } else if (strncmp(line_buffer, "sub", 3) == 0) {
      if (read_sub_instruction(line_buffer, line_number, &instruction) != 0) {
	fprintf(stderr, "Line %zi: Could not understand: %s\n", line_number, line_buffer);
	break;
      }
    } else if (strncmp(line_buffer, "jmp", 3) == 0) {
      if (read_jmp_instruction(line_buffer, line_number, &instruction) != 0) {
	fprintf(stderr, "Line %zi: Could not understand: %s\n", line_number, line_buffer);
	break;
      }
    } else if (strncmp(line_buffer, "jz", 2) == 0) {
      if (read_jz_instruction(line_buffer, line_number, &instruction) != 0) {
	fprintf(stderr, "Line %zi: Could not understand: %s\n", line_number, line_buffer);
	break;
      }
    } else if (strncmp(line_buffer, "movr", 4) == 0) {
      if (read_mov_register_instruction(line_buffer, line_number, &instruction) != 0) {
	fprintf(stderr, "Line %zi: Could not understand: %s\n", line_number, line_buffer);
	break;
      }
    } else if (strncmp(line_buffer, "movi", 4) == 0) {
      if (read_mov_immediate_instruction(line_buffer, line_number, &instruction) != 0) {
	fprintf(stderr, "Line %zi: Could not understand: %s\n", line_number, line_buffer);
	break;
      }
    } else if(strncmp(line_buffer, "lod", 3) == 0) {
      if (read_lod_instruction(line_buffer, line_number, &instruction) != 0) {
	fprintf(stderr, "Line %zi: Could not understand: %s\n", line_number, line_buffer);
      }
    } else if(strncmp(line_buffer, "str", 3) == 0) {
      if (read_str_instruction(line_buffer, line_number, &instruction) != 0) {
	fprintf(stderr, "Line %zi: Could not understand: %s\n", line_number, line_buffer);
      }
    } else if(strncmp(line_buffer, "hlt", 3) == 0) {
      instruction = 0xf0;
    } else if (strncmp(line_buffer, "db", 2) == 0) {
      if (read_db(line_buffer, line_number, &instruction) != 0) {
	fprintf(stderr, "Line %zi: Could not understand: %s\n", line_number, line_buffer);
      }
    } else {
      fprintf(stderr, "Line %zi: Could not understand: %s\n", line_number, line_buffer);
      break;
    }
    memory[memory_count++] = instruction;
  }
  return memory;
}

void save_memory(uint8_t * memory, FILE * out_file) {
  size_t write_count = 0;
  while ((write_count = fwrite(memory + write_count, sizeof(uint8_t), MEMORY_SIZE - write_count, out_file)));
}

int main(int argc, char ** argv) {
  if (argc != 3) {
    printf("Usage: foo.asm bar.bin\n");
    return -1;
  }
  FILE * in_file = fopen(argv[1], "r");
  if (in_file == NULL) {
    perror(NULL);
    return -1;
  }
  uint8_t * memory = assemble_file(in_file);
  fclose(in_file);

  FILE * out_file = fopen(argv[2], "w");
  if (out_file == NULL) {
    perror(NULL);
    return -1;
  }
  save_memory(memory, out_file);
  fclose(out_file);
  return 0;
}
