all: vm asm

vm: vm.c
	gcc -o vm vm.c -lreadline -lncurses

asm: asm.c
	gcc -o asm asm.c
